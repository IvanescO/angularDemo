import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: String;
  lastname: String;
  email: String;
  password: String;

  constructor(
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      lastname: this.lastname,
      password: this.password
    };

    let validity = true;

    if (!this.validateService.validateFirstName(user.name)) {
      this.flashMessage.show('Please enter your real name (3-16 letters, starts with a capital one)', {cssClass:'alert-danger', timeout:3000});
      validity = false;
    }

    if (!this.validateService.validateFirstName(user.lastname)) {
      this.flashMessage.show('Please enter your real lastname (3-16 letters, starts with a capital one)', {cssClass:'alert-danger', timeout:3000});
      validity = false;
    }

    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessage.show('Please use a valid email', {cssClass:'alert-danger', timeout:3000});
      validity = false;
    }

    if (!this.validateService.validatePassword(user.password)) {
      this.flashMessage.show('Please enter a password 8-32 symbols long', {cssClass:'alert-danger', timeout:3000});
      validity = false;
    }




    if (!validity) {
      return false;
    }

    // Register User
    this.authService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('You are now registered and can log in', {cssClass:'alert-success', timeout:3000});
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('Something went wrong', {cssClass:'alert-danger', timeout:3000});
        this.router.navigate(['/register']); 
      }
    });

  }

}
