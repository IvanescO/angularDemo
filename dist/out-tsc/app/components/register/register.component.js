var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
var RegisterComponent = (function () {
    function RegisterComponent(validateService, flashMessage, authService, router) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.authService = authService;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            email: this.email,
            lastname: this.lastname,
            password: this.password
        };
        var validity = true;
        if (!this.validateService.validateFirstName(user.name)) {
            this.flashMessage.show('Please enter your real name (3-16 letters, starts with a capital one)', { cssClass: 'alert-danger', timeout: 3000 });
            validity = false;
        }
        if (!this.validateService.validateFirstName(user.lastname)) {
            this.flashMessage.show('Please enter your real lastname (3-16 letters, starts with a capital one)', { cssClass: 'alert-danger', timeout: 3000 });
            validity = false;
        }
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.show('Please use a valid email', { cssClass: 'alert-danger', timeout: 3000 });
            validity = false;
        }
        if (!this.validateService.validatePassword(user.password)) {
            this.flashMessage.show('Please enter a password 8-32 symbols long', { cssClass: 'alert-danger', timeout: 3000 });
            validity = false;
        }
        if (!validity) {
            return false;
        }
        // Register User
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css']
    }),
    __metadata("design:paramtypes", [ValidateService,
        FlashMessagesService,
        AuthService,
        Router])
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=/home/mishanya/Desktop/meanrinch/angular-src/src/app/components/register/register.component.js.map